.PHONY: build

build:
	go build -o build/sender cmd/*

lint:
	golangci-lint run ./...