FROM golang:1.13-alpine as builder
COPY . /build
WORKDIR /build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
  go build -ldflags="-w -s" -mod vendor -o /go/bin/sender ./cmd/*

FROM alpine:3.10
COPY --from=builder /go/bin/sender /go/bin/sender
USER guest
WORKDIR /go/bin
CMD [ "/go/bin/sender" ]
