package core

import (
	"errors"
)

var (
	ErrConnectionFailed = errors.New("connection error")
	ErrDecodingFailed   = errors.New("encoding error")
)

type Notification struct {
	Address string `json:"address"`
	Content string `json:"content"`
}

// Message - entity returned from queue
type Message struct {
	Notification *Notification
	Error        error
}
