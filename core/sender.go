package core

import (
	"context"
	"errors"
	"log"
)

// Sender - send message for reciepient
type Sender interface {
	Send(ctx context.Context, notification *Notification) error
}

// Queue - simple queue abstraction
type Queue interface {
	Messages() <-chan Message
	Close() error
}

// Sender service implementation. Application-level code must
// implement Queue and Sender
type SenderService struct {
	Queue  Queue
	Sender Sender
}

func (s *SenderService) Serve(ctx context.Context) error {
	log.Println("waiting for messages...")
	defer s.Queue.Close()
	msgs := s.Queue.Messages()
	for {
		select {
		case <-ctx.Done():
			return nil
		case msg := <-msgs:
			if errors.Is(msg.Error, ErrConnectionFailed) {
				return msg.Error
			}
			if errors.Is(msg.Error, ErrDecodingFailed) {
				log.Println(msg.Error)
				continue
			}
			if msg.Error != nil {
				return errors.New("unexpected error")
			}
			// emulate message sending
			err := s.Sender.Send(ctx, msg.Notification)
			if err != nil {
				log.Println(err)
			}
		}
	}
}
