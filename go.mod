module sender

go 1.13

require (
	github.com/jpillora/backoff v0.0.0-20180909062703-3050d21c67d7
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/streadway/amqp v0.0.0-20190827072141-edfb9018d271
)
