// entry point to application

package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/jpillora/backoff"
	"github.com/kelseyhightower/envconfig"

	"sender/core"
	"sender/internal"
)

type Config struct {
	Queue string `default:"amqp://guest:guest@localhost:5672/"`
}

func main() {
	config := &Config{}
	_ = envconfig.Process("sender", config)

	b := &backoff.Backoff{} // exponential backoff helper

	// use signal handler and context with cancel for graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)
	go handleSignals(signals, cancel)

	log.Println("starting service...")
	defer log.Println("service stoppped")

	// infinite loop. creates/recreates service if context not cancelled
	for {
		select {
		case <-ctx.Done():
			return
		default:
			queue, err := internal.NewAMQPQueue(config.Queue)
			if err != nil {
				d := b.Duration()
				log.Printf("%s, retry after %v\n", err, d)
				time.Sleep(d)
				continue
			}
			b.Reset() // reset backoff if queue initialized successfully
			service := core.SenderService{
				Queue:  queue,
				Sender: &internal.StdoutSender{},
			}
			err = service.Serve(ctx)
			if err != nil {
				log.Println(err)
			}
		}
	}
}

// handleSignal - cancel context on signal
func handleSignals(signals chan os.Signal, cancel context.CancelFunc) {
	<-signals
	cancel()
	log.Println("stopping service...")
}
