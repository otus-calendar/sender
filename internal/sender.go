package internal

import (
	"context"
	"fmt"

	"sender/core"
)

type StdoutSender struct{}

func (s *StdoutSender) Send(ctx context.Context, n *core.Notification) error {
	fmt.Printf("Send notification to <%s>: [%s]\n", n.Address, n.Content)
	return nil
}
